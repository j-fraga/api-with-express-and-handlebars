#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Usage: launch_load_test.sh <URL> <NUM_REQUESTS>"
    exit 1
fi
URL=$1
END=$2

rm -f /tmp/launch_load_test.txt

for i in $(seq 1 "$END"); do
    echo "url = \"$URL\"" >> /tmp/launch_load_test.txt
done

echo "Sending $END requests"
curl -Ss --parallel --parallel-immediate --parallel-max 100 --config /tmp/launch_load_test.txt > /dev/null
