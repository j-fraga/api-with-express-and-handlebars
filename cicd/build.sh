#!/bin/bash

cd "$(dirname $0)" || exit 1
source cicd.cfg || exit 1
cd ..

RANDOM_SUFFIX=$(sed 's/[-]//g' < /proc/sys/kernel/random/uuid | head -c 10)
zip -9 -r --exclude=*.git* --exclude=*terraform* "${APPNAME}-${RANDOM_SUFFIX}".zip . > /dev/null 2>&1
aws s3 cp "${APPNAME}-${RANDOM_SUFFIX}".zip s3://"${BUCKET}"/ > /dev/null 2>&1
rm "${APPNAME}-${RANDOM_SUFFIX}".zip
echo "${APPNAME}-${RANDOM_SUFFIX}".zip
