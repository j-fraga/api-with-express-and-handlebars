#!/bin/bash

cd "$(dirname $0)" || exit 1
source cicd.cfg || exit 1
cd ..

# Start the node app
npm run start > app.out.log 2> app.err.log < /dev/null &
