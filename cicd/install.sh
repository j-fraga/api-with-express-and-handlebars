#!/bin/bash

cd "$(dirname $0)" || exit 1
source cicd.cfg || exit 1
cd ..

# Install node modules
npm install
