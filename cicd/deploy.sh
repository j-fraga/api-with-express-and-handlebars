#!/bin/bash

KEY=$1

cd "$(dirname $0)" || exit 1
source cicd.cfg || exit 1
cd ..

aws deploy create-deployment \
    --application-name "${APPNAME}" \
    --deployment-group-name "${DGNAME}" \
    --description "${DESCRIPTION}" \
    --s3-location bucket="${BUCKET}",bundleType="${BUNDLETYPE}",key="${KEY}" \
    --region "${REGION}"
