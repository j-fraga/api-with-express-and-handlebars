#!/bin/bash

if pidof -x "$(basename /usr/bin/node)" > /dev/null; then
    echo "Service running"
else
    echo "Service not running"
    exit 1
fi
