# API with Express & Handlebar Templates


## Architecture 

![header image](architecture.png)


## Project contents

- A copy of the code from https://github.com/nodejs/examples/tree/main/servers/express/api-with-express-and-handlebars
- `terraform` - folder containing Terraform code to provision the infrastructure
- `cicd` - folder containing build and deploy scripts to be used in the context of CI/CD
- `appspec.yml` - application specification file used by CodeDeploy to manage the deployment
- `launch_load_test.sh` - script that simulates load


## Instructions

### To deploy the infrastructure

```bash
cd terraform
terraform apply -auto-approve
```

### To deploy the application

```bash
cd cicd
./deploy.sh $(./build.sh)
```

**Note**: the `deploy.sh` script deploys an application revision to one instance at a time

### To run the load test

```bash
./launch_load_test.sh <ALB_URL> 5000
```

where ALB_URL is one of the outputs of `terraform apply`
