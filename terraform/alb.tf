# Application Load Balancer 

resource "aws_lb" "ealb" {
  name               = "${var.app_name}-ASG-ALB"
  internal           = false
  load_balancer_type = "application"
  subnets            = [aws_subnet.pub-subnet-1a.id, aws_subnet.pub-subnet-1b.id, aws_subnet.pub-subnet-1c.id]
  security_groups    = [aws_security_group.ealb_sg.id]
}

resource "aws_lb_target_group" "alb_tg_app1" {
  name_prefix          = "ALB1-"
  port                 = 3000
  protocol             = "HTTP"
  deregistration_delay = 30
  vpc_id               = aws_vpc.app_vpc.id

  health_check {
    path              = "/"
    port              = "traffic-port"
    healthy_threshold = 2
    interval          = 10
  }
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.ealb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.alb_tg_app1.arn
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "alb_rules" {
  listener_arn = aws_lb_listener.alb_listener.arn

  priority = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg_app1.arn
  }
  condition {
    path_pattern {
      values = ["/app1/*"]
    }
  }
}

# Security group for the ALB

resource "aws_security_group" "ealb_sg" {
  name        = "${var.app_name}-LB-SG"
  description = "load balancer security group"
  vpc_id      = aws_vpc.app_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
