# IAM role for EC2 instances

resource "aws_iam_policy" "ec2instance_policy" {
  name   = "ec2instance_policy"
  path   = "/"
  policy = data.aws_iam_policy_document.ec2_instance_role_policies.json
}

resource "aws_iam_role" "ec2instance_role" {
  name               = "ec2instance_role"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "ec2instance_policy_attach" {
  role       = aws_iam_role.ec2instance_role.name
  policy_arn = resource.aws_iam_policy.ec2instance_policy.arn
}

resource "aws_iam_instance_profile" "ec2instance_cd_instance_profile" {
  name = "ec2instance_cd_instance_profile"
  role = aws_iam_role.ec2instance_role.name
}


# Code Deploy role

resource "aws_iam_role" "codedeploy_role" {
  name = "codedeploy_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "codedeploy_attach1" {
  role       = aws_iam_role.codedeploy_role.name
  policy_arn = data.aws_arn.AWSCodedeploy.arn
}

resource "aws_iam_role_policy_attachment" "codedeploy_attach2" {
  role       = aws_iam_role.codedeploy_role.name
  policy_arn = data.aws_arn.AutoScalingFullAccess.arn
}

resource "aws_iam_role_policy_attachment" "codedeploy_attach3" {
  role       = aws_iam_role.codedeploy_role.name
  policy_arn = data.aws_arn.CloudWatchFullAccess.arn
}

resource "aws_iam_role_policy_attachment" "codedeploy_attach4" {
  role       = aws_iam_role.codedeploy_role.name
  policy_arn = data.aws_arn.AmazonEC2FullAccess.arn
}
