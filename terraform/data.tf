# To get the effective Account ID

data "aws_caller_identity" "current" {}


# EC2 AMI

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}


# ARNs

data "aws_arn" "AWSCodedeploy" {
  arn = var.AWSCodedeploy_arn
}

data "aws_arn" "AmazonEC2FullAccess" {
  arn = var.AmazonEC2FullAccess_arn
}

data "aws_arn" "AmazonS3ReadOnlyAccess" {
  arn = var.AmazonS3ReadOnlyAccess_arn
}

data "aws_arn" "AmazonS3FullAccess" {
  arn = var.AmazonS3FullAccess_arn
}

data "aws_arn" "AutoScalingFullAccess" {
  arn = var.AutoScalingFullAccess_arn
}

data "aws_arn" "CloudWatchFullAccess" {
  arn = var.CloudWatchFullAccess_arn
}


# Policies

data "aws_iam_policy_document" "ec2_assume_role_policy_doc" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "ec2_instance_role_policies" {
  statement {
    sid       = "AllowReadingParameterStore1"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "ssm:DescribeParameters"
    ]
  }

  statement {
    sid       = "AllowReadingParameterStore2"
    effect    = "Allow"
    resources = ["arn:aws:ssm:*:*:parameter/cloudwatch-agent/config"]
    actions = [
      "ssm:GetParameter",
    ]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "cloudwatch:PutMetricData",
      "ec2:DescribeVolumes",
      "ec2:DescribeTags",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams",
      "logs:DescribeLogGroups",
      "logs:CreateLogStream",
      "logs:CreateLogGroup"
    ]
  }

  statement {
    sid    = "AllowReadFromArtifactsBucket"
    effect = "Allow"
    resources = [
      "${resource.aws_s3_bucket.deploy-artifacts.arn}/*",
      "${resource.aws_s3_bucket.deploy-artifacts.arn}"
    ]
    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetEncryptionConfiguration",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:ListMultipartUploadParts"
    ]
  }
}
