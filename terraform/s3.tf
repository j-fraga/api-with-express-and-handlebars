resource "aws_s3_bucket" "deploy-artifacts" {
  bucket        = "${var.app_name}-${data.aws_caller_identity.current.account_id}"
  acl           = "private"
  force_destroy = true
}
