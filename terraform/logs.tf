# Logs

resource "aws_cloudwatch_log_group" "system_logs" {
  name              = "${var.app_name}-SystemsLogs"
  retention_in_days = var.log_group_retention_in_days
}

resource "aws_cloudwatch_log_group" "application_logs" {
  name              = "${var.app_name}-ApplicationLogs"
  retention_in_days = var.log_group_retention_in_days
}


# Cloudwatch agent

locals {
  userdata = templatefile("ec2-user-data.sh.template", {
    ssm_cloudwatch_config = aws_ssm_parameter.cw_agent.name
  })

  cw_agent_config = templatefile("cw_agent_config.json.template", {
    appname = var.app_name
  })
}

resource "aws_ssm_parameter" "cw_agent" {
  description = "Cloudwatch agent config to configure custom log"
  name        = "/cloudwatch-agent/config"
  type        = "String"
  value       = local.cw_agent_config
}
