provider "aws" {
  profile = "default"
  region  = var.region
}

variable "region" {
  default = "eu-west-3"
}

variable "app_name" {
  description = "Prefix to all generated AWS resource names"
  default     = "demoapp"
}

variable "key_name" {
  default = "joao-fraga-aws"
}

variable "ec2_instancetype" {
  default = "t2.micro"
}

variable "log_group_retention_in_days" {
  default = 30
}


# ARNs

variable "AmazonEC2FullAccess_arn" {
  default = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

variable "AmazonS3FullAccess_arn" {
  default = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

variable "AWSCodedeploy_arn" {
  default = "arn:aws:iam::aws:policy/AWSCodeDeployFullAccess"
}

variable "AmazonS3ReadOnlyAccess_arn" {
  default = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}

variable "AutoScalingFullAccess_arn" {
  default = "arn:aws:iam::aws:policy/AutoScalingFullAccess"
}

variable "CloudWatchFullAccess_arn" {
  default = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
}
