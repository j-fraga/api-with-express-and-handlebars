resource "aws_launch_configuration" "app_launch_conf" {
  name_prefix                 = "apps-"
  iam_instance_profile        = aws_iam_instance_profile.ec2instance_cd_instance_profile.name
  image_id                    = data.aws_ami.amazon-linux-2.id
  instance_type               = var.ec2_instancetype
  key_name                    = var.key_name
  security_groups             = [aws_security_group.asg_app_sg.id]
  associate_public_ip_address = true
  user_data                   = local.userdata

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "app_asg" {
  name                 = "app_asg"
  launch_configuration = aws_launch_configuration.app_launch_conf.name
  min_size             = 3
  desired_capacity     = 3
  max_size             = 5
  health_check_type    = "EC2"
  target_group_arns = [
    aws_lb_target_group.alb_tg_app1.arn
  ]
  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  vpc_zone_identifier = [
    aws_subnet.pub-subnet-1a.id,
    aws_subnet.pub-subnet-1b.id,
    aws_subnet.pub-subnet-1c.id
  ]

  lifecycle {
    create_before_destroy = true
  }

}

# ASG scale-up policy

resource "aws_autoscaling_policy" "app_asg_policy_up" {
  name                   = "app_asg_policy_up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.app_asg.name
}

resource "aws_cloudwatch_metric_alarm" "app_asg_network_packets_out_alarm_up" {
  alarm_name          = "app_asg_network_packets_out_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkPacketsOut"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "500"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.app_asg.name
  }

  alarm_description = "This metric shows the number of packets sent by the instance on all network interfaces"
  alarm_actions     = ["${aws_autoscaling_policy.app_asg_policy_up.arn}"]
}

# ASG scale-down policy

resource "aws_autoscaling_policy" "app_asg_policy_down" {
  name                   = "app_asg_policy_down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 120
  autoscaling_group_name = aws_autoscaling_group.app_asg.name
}

resource "aws_cloudwatch_metric_alarm" "app_asg_network_packets_out_alarm_down" {
  alarm_name          = "app_asg_network_packets_out_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkPacketsOut"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "150"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.app_asg.name
  }

  alarm_description = "This metric shows the number of packets sent by the instance on all network interfaces"
  alarm_actions     = [aws_autoscaling_policy.app_asg_policy_down.arn]
}


# Security group for ASG 

resource "aws_security_group" "asg_app_sg" {
  name        = "asg_app_sg"
  description = "Allow HTTP inbound connections"
  vpc_id      = aws_vpc.app_vpc.id

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
