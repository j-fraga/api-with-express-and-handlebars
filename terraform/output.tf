output "public_dns" {
  value = aws_lb.ealb.dns_name
}

output "deploy-artifacts-bucket" {
  value = aws_s3_bucket.deploy-artifacts.bucket
}

output "codedeploy-application-name" {
  value = aws_codedeploy_app.demo_app.name
}