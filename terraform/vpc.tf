# VPC

resource "aws_vpc" "app_vpc" {
  cidr_block           = "10.10.0.0/21"
  enable_dns_hostnames = "true"
}


# IGW

resource "aws_internet_gateway" "app_internet_gateway" {
  vpc_id = aws_vpc.app_vpc.id
}


# Subnets

resource "aws_subnet" "pub-subnet-1a" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.0.0/24"
  availability_zone = "${var.region}a"
}

resource "aws_subnet" "prv-subnet-1a" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = "${var.region}a"
}

resource "aws_subnet" "pub-subnet-1b" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.2.0/24"
  availability_zone = "${var.region}b"
}

resource "aws_subnet" "prv-subnet-1b" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.3.0/24"
  availability_zone = "${var.region}b"
}

resource "aws_subnet" "pub-subnet-1c" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.4.0/24"
  availability_zone = "${var.region}c"
}

resource "aws_subnet" "prv-subnet-1c" {
  vpc_id            = aws_vpc.app_vpc.id
  cidr_block        = "10.10.5.0/24"
  availability_zone = "${var.region}c"
}


# Route tables

resource "aws_route_table" "app_pub_route_table" {
  vpc_id = aws_vpc.app_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.app_internet_gateway.id
  }
}

resource "aws_route_table_association" "app_table_associatio_2a" {
  subnet_id      = aws_subnet.pub-subnet-1a.id
  route_table_id = aws_route_table.app_pub_route_table.id
}

resource "aws_route_table_association" "app_table_association_2b" {
  subnet_id      = aws_subnet.pub-subnet-1b.id
  route_table_id = aws_route_table.app_pub_route_table.id
}

resource "aws_route_table_association" "app_table_association_2c" {
  subnet_id      = aws_subnet.pub-subnet-1c.id
  route_table_id = aws_route_table.app_pub_route_table.id
}


# Security group

resource "aws_security_group" "app_security_group" {
  vpc_id = aws_vpc.app_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
